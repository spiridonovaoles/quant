
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package quant.beans;

import java.util.ArrayList;
import quant.emulator.QuantumRegister;
// Этот класс позволяет хранить квантовые схемы и предлагает методы для выполнения квантовых операций на них. 
// Квантовая схема состоит из последовательности квантовых гейтов.

public class Circuit extends ArrayList<GateElement> implements java.io.Serializable {

    public static String[] gatelist = {
        "initialState",
        "HadamardGate",
        "cNOTgate",
        "XGate",
        "YGate",
        "ZGate",
        "SGate",
        "TGate",     
        "ToffoliGate"       
    };

    public static String[] gateListInNewBasis = {
        "QET",
        "PHASE",
        "CQET"
    };

    public QuantumRegister register;

    public int registerSize = 0;

    public int numberOfWires = 0; // проводочки)

    public int nextGateNumber = 0;

    /**
     * Пустая кв. схема
     */
    public Circuit() {
        super();
    }

    // размер x-регистра этой квантовой схемы
    public int getXRegisterSize() {
        return registerSize;
    }

    public int getNumberOfWires() {
        return numberOfWires;
    }

    //нач. состояния кубит в квантовой схемы
    public void setInitialQubits(int[] initialQubits) {
        setInitialState(initialQubits);
    }

    public GateElement getNextGate() {
        return get(nextGateNumber);
    }

    public GateElement getPreviousGate() {
        return get(nextGateNumber - 1);
    }

    public int getNextGateNumber() {
        return nextGateNumber;
    }

    public QuantumRegister getRegister() {
        return register;
    }

    public void setRegister(QuantumRegister register) {
        this.register = register;
    }

    public int getRegisterSize() {
        return registerSize;
    }

    public void setRegisterSize(int registerSize) {
        this.registerSize = registerSize;
    }

    // список квантовых гейтов этой квантовой схемы.
    public ArrayList<GateElement> getGates() {
        return new ArrayList<GateElement>(this);
    }

    /**
     * Формирует и инициализирует эту квантовую схему в соответствии с заданными
     * размерами регистров и перечнем квантовых гейтов. Все ранее сохраненные
     * квантовые гейты этой схемы будут удалены.
     */
    public void initialize(int[] registerSizes, ArrayList<GateElement> gates) throws Exception {
        this.registerSize = registerSizes[0];
        this.numberOfWires = registerSize;

        clear();
        addAll(gates);
        if (size() > 0) {
            nextGateNumber = 1;
        }

        initializeRegisters();
    }

   

    public void initializeRegisters() throws Exception {
        if (size() == 0) {
            return;
        }

        int[] initialQubits = get(0).getQubits();
        register = new QuantumRegister(registerSize);
        for (int i = 1; i <= registerSize; i++) {
            if (initialQubits[registerSize - i] == 1) {
                register.XGate(i);
            }
        }

        nextGateNumber = 1;

    }

    // Устанавливает квантовую схему в конечное состояние.
    // с помощью этого метода весь квантовый алгоритм, реализованный с помощью этой схемы выполняется.
    public void setFinalStep() {
        nextGateNumber = this.size();
    }

    // выполняет след гейт
    public void setNextStep() throws Exception {
        if (nextGateNumber < this.size()) {
            performGate(get(nextGateNumber));
            nextGateNumber++;
        }
    }

    // Выполняет обратное ранее выполненному гейту в этой схеме
    public void setPreviousStep() throws Exception {
        if (nextGateNumber > 1) {
            nextGateNumber--;
            unperformGate(get(nextGateNumber));
        }
    }

    // Определяет начальное состояние квантовой схемы. 
    // initialQubits массив содержит индексы кубитов которым присвоено значение |0> или |1>.
    // То есть, начальное состояние соответствует классическому состоянию бита.
    public void setInitialState(int[] initialQubits) {
        if (this.size() == 0) {
            this.add(0, new GateElement("initialState", initialQubits));
        } else {
            this.set(0, new GateElement("initialState", initialQubits));
        }
        nextGateNumber = 1;
    }

    public void addHadamard(int i) throws PureException {
        if (numberOfWires == 0) {
            throw new PureException("Регистр не существует");
        }
        this.add(new GateElement("HadamardGate", new int[]{i}));
    }

    public void addCNOT(int[] qubits) throws PureException {
        if (numberOfWires == 0) {
            throw new PureException("Регистр не существует");
        }
        this.add(new GateElement("cNOTgate", qubits));
    }

    public void addPauliX(int i) throws PureException {
        if (numberOfWires == 0) {
            throw new PureException("Регистр не существует");
        }
        this.add(new GateElement("XGate", new int[]{i}));
    }

    public void addPauliY(int i) throws PureException {
        if (numberOfWires == 0) {
            throw new PureException("Регистр не существует");
        }
        this.add(new GateElement("YGate", new int[]{i}));
    }

    public void addPauliZ(int i) throws PureException {
        if (numberOfWires == 0) {
            throw new PureException("Регистр не существует");
        }
        this.add(new GateElement("ZGate", new int[]{i}));
    }

    public void addSGate(int i) throws PureException {
        if (numberOfWires == 0) {
            throw new PureException("Регистр не существует");
        }
        this.add(new GateElement("SGate", new int[]{i}));
    }

    
    public void addTGate(int i) throws PureException {
        if (numberOfWires == 0) {
            throw new PureException("Регистр не существует");
        }
        this.add(new GateElement("TGate", new int[]{i}));
    }

    public void addToffoli(int[] qubits) throws PureException {
        if (numberOfWires == 0) {
            throw new PureException("Регистр не существует");
        }
        this.add(new GateElement("ToffoliGate", qubits));
    }

    //выполняет всю схему и возвращает true если все ок
    public boolean executeAll(QuantumRegister reg) throws Exception {
        register = reg;
        for (int i = 0; i < size(); i++) {
            performGate(get(i));
        }
        setFinalStep();
        return true;//ура!!!
    }

    //выполняет конкретный гейт схемы
    private void performGate(GateElement gate) throws Exception {
        if (gate.getName().equalsIgnoreCase("HadamardGate")) {
            int qubit = gate.qubits[0];
            {
                register.hadamardGate(qubit);
            }
        } else if (gate.getName().equalsIgnoreCase("cNOTgate")) {
            {
                register.cNOTgate(gate.qubits[0], gate.qubits[1]);
            }
        } else if (gate.getName().equalsIgnoreCase("XGate")) {
            int qubit = gate.qubits[0];
            {
                register.XGate(qubit);
            }
        } else if (gate.getName().equalsIgnoreCase("YGate")) {
            int qubit = gate.qubits[0];
            {
                register.YGate(qubit);
            }
        } else if (gate.getName().equalsIgnoreCase("ZGate")) {
            int qubit = gate.qubits[0];
            {
                register.ZGate(qubit);
            }
        } else if (gate.getName().equalsIgnoreCase("SGate")) {
            int qubit = gate.qubits[0];
            {
                register.sGate(qubit);
            }
        } else if (gate.getName().equalsIgnoreCase("TGate")) {
            int qubit = gate.qubits[0];
            {
                register.tGate(qubit);
            }
        } else if (gate.getName().equalsIgnoreCase("ToffoliGate")) {
            {
                register.toffoliGate(gate.qubits[0], gate.qubits[1], gate.qubits[2]);
            }
        } else {
            throw new IllegalArgumentException("Неопознанный гейт " + gate.getName());
        }
    }

    // обратное переданному гейту
    private void unperformGate(GateElement gate) throws Exception {
        if (gate.getName().equalsIgnoreCase("HadamardGate")) {
            int qubit = gate.qubits[0];
            {
                register.hadamardGate(qubit);
            }
        } else if (gate.getName().equalsIgnoreCase("cNOTgate")) {
            {
                register.cNOTgate(gate.qubits[0], gate.qubits[1]);
            }
        } else if (gate.getName().equalsIgnoreCase("XGate")) {
            int qubit = gate.qubits[0];
            {
                register.XGate(qubit);
            }
        } else if (gate.getName().equalsIgnoreCase("YGate")) {
            int qubit = gate.qubits[0];
            {
                register.YGate(qubit);
            }
        } else if (gate.getName().equalsIgnoreCase("ZGate")) {
            int qubit = gate.qubits[0];
            {
                register.ZGate(qubit);
            }
        } else if (gate.getName().equalsIgnoreCase("inverseSGate")) {
            int qubit = gate.qubits[0];
            {
                register.sGate(qubit);
            }
        } else if (gate.getName().equalsIgnoreCase("ToffoliGate")) {
            {
                register.toffoliGate(gate.qubits[0], gate.qubits[1], gate.qubits[2]);
            }
        } else {
            throw new IllegalArgumentException("Неопознанный гейт " + gate.getName());
        }
    }
}
