/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package quant.beans;

import java.util.Random;

/**
 *
 * @author Asus
 */
public class QuantumMistake {

    // гейт-ошибка
    public GateElement gate;

    // вероятность применения гейта-ошибки
    public double probability;

    public GateElement getGate() {
        return gate;
    }

    public void setGate(GateElement gate) {
        this.gate = gate;
    }

    public double getProbability() {
        return probability;
    }

    public void setProbability(double probability) {
        this.probability = probability;
    }

    public void addMistakeOnStep(double probability, Circuit circuit, int quantInd) {//вероятность возникновения гейта-ошибки в схеме
        if (Math.random() < probability) {
            circuit.add(quantInd, gate);
        }
    }
    // у гейта уже определены индексы кубитов на которых он работает
    public void addMistakeOnRandomStep(double probability, Circuit circuit) {//вероятность возникновения гейта-ошибки в схеме
        if (Math.random() < probability) {
            Random rand = new Random(); // еще и в случайном месте)) но это пока
            int quantInd = rand.nextInt((circuit.size() - 0) + 1) + 0;
            circuit.add(quantInd, gate);
        }
    }

}
