/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package quant.beans;

/**
 *
 * @author Asus
 */
public class GateElement implements java.io.Serializable {

    private static final long serialVersionUID = 1488467558;
    // имя гейта
    public String name;
    /**
     * определяет индекс кубитов, на котором работает гейт       Для управляемых
     * гейтов, последний в масиве будет целевым кубитом.
     */
    public int[] qubits;

    public GateElement(String name, int[] qubits) {
        if (!isValid(name)) {
            throw new IllegalArgumentException("Попытка создать несуществующий гейт " + name);
        }
        this.name = name;
        this.qubits = qubits;

    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int[] getQubits() {
        return qubits;
    }

    public void setQubits(int[] qubits) {
        this.qubits = qubits;
    }

    public static boolean isValid(String name) {
        boolean valid = false;
        for (int i = 0; !valid && i < Circuit.gatelist.length; i++) {
            valid = name.equals(Circuit.gatelist[i]);
        }
        return valid;
    }
}
