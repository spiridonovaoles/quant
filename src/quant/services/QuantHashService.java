/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package quant.services;

import java.util.ArrayList;
import java.util.List;
import quant.emulator.Complex;
import quant.emulator.QuantumRegister;

/**
 *
 * @author Asus
 */
public class QuantHashService {

    // возвращает квантовое хэширование
    // bitString - входной массив классических бит (строка), algAccuracy погрешность самого алгоритма
    public QuantumRegister quantumHashAlgorithm(String bitString, double algAccuracy) throws Exception {
        int n = bitString.trim().length();
        int[] bits = new int[n];//масив w
        for (int j = 0; j < n; j++) {
            if (!Character.isDigit(bitString.charAt(j))) {
                System.out.println("Некорректный формат числа!");
                break;
            }
            bits[j] = Integer.parseInt(String.valueOf(bitString.charAt(j)));

        }

        double dVar = n / (Math.pow(algAccuracy, 2));
        int d = (int) Math.round(dVar);//сгенерируем массив B
        double[] bArr = new double[d];
        for (int k = 0; k < d; k++) {
            int varB = (int) (Math.round(Math.random() * (2)));
            if (varB > 1) {
                varB = 1;
            }
            bArr[k] = varB;
        }//конец генерации B
        List<Complex> quantumHash = new ArrayList();
        for (int j = 0; j < n; j++) {
            double sum = 0;
            Complex complexFactor = Complex.zero();
            for (int l = 0; l < d; l++) {
                sum = sum + Math.pow((-1), (bArr[l] * bits[j]));
                complexFactor = new Complex(bArr[l], 0.); //множитель |i>
                complexFactor = complexFactor.multiply(sum);
            }
            complexFactor = complexFactor.multiply(Math.sqrt(d)); //получили одно состояние
            quantumHash.add(complexFactor);
        }
        QuantumRegister register = new QuantumRegister(quantumHash.size());
        int ind = 0;
        for (Complex quant : quantumHash) {
            register.setQubitsNumberValue(ind, new Complex(quant.getImaginary(), quant.getImaginary()));
            register.QET(Math.PI, ind);
            register.phase(Math.PI, ind);
            ind++;
        }
        return register;
    }
}
