/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package quant.services;

import quant.emulator.Complex;
import quant.emulator.QuantumRegister;

/**
 *
 * @author Asus
 */
public class QuatumXMistakeCorrectService {

    public QuantumRegister codingXInputRegister(QuantumRegister register) throws Exception {
        QuantumRegister answerRegister = new QuantumRegister(register.getQubitsNumber());
        int jMax = register.getQubitsNumber();
        int k = 0;
        int controlIndex1;
        int targetIndex1;
        int targetIndex2;
        for (int j = 0; j < jMax; j++) {
            controlIndex1 = k;
            answerRegister.setQubitsNumberValue(k, register.getQubitsNumberValue(j));
            k++;
            targetIndex1 = k;
            answerRegister.setQubitsNumberValue(k, Complex.zero());
            k++;
            targetIndex2 = k;
            answerRegister.setQubitsNumberValue(k, Complex.zero());
            k++;
        }
        for (int j = 0; j < answerRegister.getQubitsNumber(); j++) {
            controlIndex1 = j;
            answerRegister.setQubitsNumberValue(j, register.getQubitsNumberValue(j));
            j++;
            targetIndex1 = j;
            answerRegister.setQubitsNumberValue(j, Complex.zero());
            j++;
            targetIndex2 = j;
            answerRegister.setQubitsNumberValue(j, Complex.zero());
            j++;
            answerRegister.cNOTgate(controlIndex1, targetIndex1);
            answerRegister.cNOTgate(controlIndex1, targetIndex2); //закодируем по трехкубитной схеме

        }
        return answerRegister;
    }

    public QuantumRegister decodingXInputRegister(QuantumRegister register) throws Exception {
        QuantumRegister answerRegister = new QuantumRegister(register.getQubitsNumber());
        int index1;
        int index2;
        int index3;
        for (int j = 0; j < register.getQubitsNumber(); j++) {
            index1 = j;
            answerRegister.setQubitsNumberValue(j, register.getQubitsNumberValue(j));
            j++;
            index2 = j;
            answerRegister.setQubitsNumberValue(j, Complex.zero());
            j++;
            index3 = j;
            answerRegister.setQubitsNumberValue(j, Complex.zero());
            j++;
            answerRegister.cNOTgate(index1, index3);
            answerRegister.cNOTgate(index1, index2);
            answerRegister.toffoliGate(index3, index2, index1); //разкодируем по трехкубитной схеме

        }
        return answerRegister;
    }

    public QuantumRegister methodStina(QuantumRegister register) throws Exception {
        QuantumRegister answerRegister = new QuantumRegister(register.getQubitsNumber());
        int jMax = register.getQubitsNumber();
        int k = 0;
        int controlIndex1;
        int targetIndex1;
        int targetIndex2;
        for (int j = 0; j < jMax; j++) {
            controlIndex1 = k;
            answerRegister.setQubitsNumberValue(k, register.getQubitsNumberValue(j));
            k++;
            targetIndex1 = k;
            answerRegister.setQubitsNumberValue(k, Complex.zero());
            k++;
            targetIndex2 = k;
            answerRegister.setQubitsNumberValue(k, Complex.zero());
            k++;
        }
        for (int j = 0; j < answerRegister.getQubitsNumber(); j++) {
            controlIndex1 = j;
            answerRegister.setQubitsNumberValue(j, register.getQubitsNumberValue(j));
            j++;
            targetIndex1 = j;
            answerRegister.setQubitsNumberValue(j, Complex.zero());
            j++;
            targetIndex2 = j;
            answerRegister.setQubitsNumberValue(j, Complex.zero());
            j++;
            answerRegister.cNOTgate(controlIndex1, targetIndex1);
            answerRegister.cNOTgate(controlIndex1, targetIndex2); //закодируем по трехкубитной схеме

        }
        return answerRegister;
    }
}
