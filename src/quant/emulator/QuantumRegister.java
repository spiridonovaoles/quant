package quant.emulator;

import java.util.Random;
import quant.gates.PhaseGate;

public class QuantumRegister {

    private int qubitsNumber;
    private int size;
    private Complex[] vector;

    public QuantumRegister(int qubitsNumber) {
        this.setQubitsNumber(qubitsNumber);
    }

    public QuantumRegister(int qubitsNumber, Complex[] vector) throws Exception {
        this.qubitsNumber = qubitsNumber;
        size = ((int) Math.pow(2, qubitsNumber));
        this.vector = vector;
        if (size != vector.length) {
            throw new Exception();
        }
    }

    public Complex[] getVector() {
        return vector;
    }

    public int getQubitsNumber() {
        return qubitsNumber;
    }

    public void setQubitsNumber(int qubitsNumber) {
        this.qubitsNumber = qubitsNumber;
        this.size = ((int) Math.pow(2, qubitsNumber));
        this.vector = new Complex[size];
        this.vector[0] = Complex.unit();
        for (int i = 1; i < this.vector.length; i++) {
            vector[i] = Complex.zero();
        }
    }

    public Complex getQubitsNumberValue(int qubitIndex) {
        return this.vector[qubitIndex];
    }

    public void setQubitsNumberValue(int qubitIndex, Complex complex) {
        this.vector[qubitIndex] = complex;
    }

    public void multiplyOnMatrix(Complex[][] matrix) throws Exception {
        if (matrix.length != size) {
            throw new Exception();
        }
        this.vector = ComplexMath.multiplication(matrix, size, vector);
    }

    public String toString() {
        String result = "|psi> = ";
        boolean first = true;
        for (int i = 0; i < size; i++) {
            String binary = "";
            int k = i;
            while (k > 0) {
                binary = k % 2 + binary;
                k /= 2;
            }
            int length = binary.length();
            for (int j = qubitsNumber; j > length; j--) {
                binary = "0" + binary;
            }
            if (!first) {
                result += " + ";
            }
            result = result + vector[i] + " |" + binary + "> ";
            first = false;
        }
        return result;
    }

    public void performAlgorythm(QuantumAlgorithm algorythm) throws Exception {
        vector = ComplexMath.multiplication(algorythm.getMatrix(), size, vector);
    }

    public void performAlgorythm(OneStepOneQubitGateAlgorythm algorythm) throws Exception {
        vector = ComplexMath.multiplication(algorythm.getMatrix(), size, vector);
    }

/// Измерение
    public int measureQubit(int qubit, boolean needIncreaseQubitsNumber) throws Exception {
        if (qubit >= qubitsNumber) {
            throw new Exception();
        }
        Complex[][] P0 = ComplexMath.zeroMatrix(size, size);
        int pow2n_q = (int) Math.pow(2, qubitsNumber - qubit);
        int pow2n_q_1 = (int) Math.pow(2, qubitsNumber - qubit - 1);
        // нужно пройти по всем состояниям, где текущий кубит 0
        for (int i = 0; i < size; i += pow2n_q) {
            for (int j = i; j < i + pow2n_q_1; j++) {
                P0[j][j] = Complex.unit();
            }
        }
        Complex[][] vectorBra = new Complex[1][size];
        Complex[][] vectorKet = new Complex[size][1];
        for (int i = 0; i < size; i++) {
            vectorKet[i][0] = vector[i];
        }
        vectorBra[0] = vector.clone();
        Complex[][] p0 = ComplexMath.matricesMultiplication(vectorBra, 1, size,
                P0, size, size);
        p0 = ComplexMath.matricesMultiplication(p0, 1, size, vectorKet, size, 1);
        double p0Norma = p0[0][0].mod();
        int result = 0;

        //measure and normalize
        Complex[][] Pm;
        if (new Random().nextDouble() > p0Norma) {
            result = 1;
//            Configure P1 projector
            Pm = ComplexMath.zeroMatrix(size, size);
            for (int i = pow2n_q_1; i < size; i += pow2n_q) {
                for (int j = i; j < i + pow2n_q_1; j++) {
                    Pm[j][j] = Complex.unit();
                }
            }
        } else {
            Pm = P0;
        }

        //norm
        vector = ComplexMath.multiplication(Pm, size, vector);
        double norma = 0.0;
        for (int i = 0; i < size; i++) {
            norma += Math.pow(vector[i].mod(), 2);
        }
        norma = Math.sqrt(norma);

        Complex complexNorma = new Complex(norma, 0);

        for (int i = 0; i < size; i++) {
            vector[i] = Complex.devide(vector[i], complexNorma);
        }

        if (needIncreaseQubitsNumber) {
            int oldSize = size;
            size /= 2;
            qubitsNumber--;

            Complex[] newVector = new Complex[size];
            int firstIndex = result == 0 ? 0 : pow2n_q_1;

            int z = 0;

            for (int i = firstIndex; i < oldSize; i += pow2n_q) {
                for (int j = i; j < i + pow2n_q_1; j++) {
                    newVector[z] = vector[j];
                    z++;
                }
            }

            vector = newVector;
        }

        return result;
    }

    public void phase(double thetaInRadians, int qubitAddressInRegister) throws Exception {
        OneStepOneQubitGateAlgorythm oneStepOneQubitGateAlgorythm = new OneStepOneQubitGateAlgorythm(getQubitsNumber(),
                new PhaseGate(thetaInRadians),
                qubitAddressInRegister
        );
        performAlgorythm(oneStepOneQubitGateAlgorythm);
    }

    public void QET(double thetaInRadians, int qubitAddressInRegister) throws Exception {
        Complex[][] matrix = {
            {new Complex(Math.cos(thetaInRadians / 2), 0), new Complex(0, Math.sin(thetaInRadians / 2))},
            {new Complex(0, Math.sin(thetaInRadians / 2)), new Complex(Math.cos(thetaInRadians / 2), 0)}
        };
        OneStepOneQubitGateAlgorythm oneStepOneQubitGateAlgorythm = new OneStepOneQubitGateAlgorythm(getQubitsNumber(),
                matrix,
                qubitAddressInRegister
        );
        performAlgorythm(oneStepOneQubitGateAlgorythm);
    }

    public void cQET(double thetaInRadians, int controllingQubitAddressInRegister, int controlledQubitAddressInRegister) throws Exception {
        Complex[][] matrix = {
            {new Complex(Math.cos(thetaInRadians / 2), 0), new Complex(0, Math.sin(thetaInRadians / 2)), Complex.zero(), Complex.zero()},
            {new Complex(0, Math.sin(thetaInRadians / 2)), new Complex(Math.cos(thetaInRadians / 2), 0), Complex.zero(), Complex.zero()},
            {Complex.zero(), Complex.zero(), Complex.unit(), Complex.zero()},
            {Complex.zero(), Complex.zero(), Complex.zero(), Complex.unit()}
        };
        OneStepTwoQubitGateAlgorythm algorythm = new OneStepTwoQubitGateAlgorythm(
                getQubitsNumber(),
                controllingQubitAddressInRegister,
                controlledQubitAddressInRegister,
                matrix
        );
        performAlgorythm(algorythm);
    }

    // Адамара
    public void hadamardGate(int qubitAddressInRegister) throws Exception {
        QET(-(Math.PI / 2), qubitAddressInRegister);
        phase((Math.PI / 2), qubitAddressInRegister);
        QET(-(Math.PI / 2), qubitAddressInRegister);
    }

    // c-NOT, j - контролирующий кубит, k - кубит-мишень
    public void cNOTgate(int controllingQubitAddressInRegister, int controlledQubitAddressInRegister) throws Exception {
        QET(Math.PI, controllingQubitAddressInRegister);
        QET(Math.PI, controlledQubitAddressInRegister);
    }

    // X гейт (как NOT классический)
    public void XGate(int qubitAddressInRegister) throws Exception {
        QET(Math.PI, qubitAddressInRegister);
    }

    // Y гейт
    public void YGate(int qubitAddressInRegister) throws Exception {
        QET(Math.PI, qubitAddressInRegister);
        phase(Math.PI, qubitAddressInRegister);
    }

    // Z гейт
    public void ZGate(int qubitAddressInRegister) throws Exception {
        phase(Math.PI, qubitAddressInRegister);
    }

    // S гейт - фазовый гейт
    public void sGate(int qubitAddressInRegister) throws Exception {
        phase((Math.PI / 2), qubitAddressInRegister);
    }

    // T гейт - pi/8 гейт
    public void tGate(int qubitAddressInRegister) throws Exception {
        phase((Math.PI / 4), qubitAddressInRegister);
    }


    /* гейт Тоффоли
     *  @param j1 - первый контролирующий кубит
     *  @param j2 - второй контролирующий кубит
     *  @param k - кубит-мишень
     */
    public void toffoliGate(int controllingQubit1AddressInRegister, int controllingQubit2AddressInRegister, int controlledQubitAddressInRegister) {
    }
}
