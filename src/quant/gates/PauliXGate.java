package quant.gates;

import quant.emulator.Complex;
import quant.emulator.QuantumGate;



public class PauliXGate extends QuantumGate {
    public PauliXGate (){
        this.qubitsNumber=1;
        this.size=2;
    }
    @Override
    public Complex[][] getMatrix() {
        Complex result [][] = {
                {Complex.zero(),Complex.unit()},
                {Complex.unit(),Complex.zero()}
        };
        return result;
    }
}
