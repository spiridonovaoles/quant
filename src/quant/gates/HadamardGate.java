package quant.gates;

import quant.emulator.Complex;
import quant.emulator.QuantumGate;


public class HadamardGate extends QuantumGate {
    public HadamardGate (){
        this.qubitsNumber=1;
        this.size=2;
    }
    @Override
    public Complex[][] getMatrix() {
        Complex result [][] = {
                {new Complex(1/Math.sqrt(2), 0),new Complex(1/Math.sqrt(2),0)},
                {new Complex(1/Math.sqrt(2), 0),new Complex(-1/Math.sqrt(2),0)}
        };
        return result;
    }
}
