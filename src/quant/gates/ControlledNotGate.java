package quant.gates;

import quant.emulator.Complex;
import quant.emulator.QuantumGate;


public class ControlledNotGate extends QuantumGate {
    public ControlledNotGate (){
        this.qubitsNumber=2;
        this.size=4;
    }
    @Override
    public Complex[][] getMatrix() {
        Complex result [][] = {
                {Complex.unit(), Complex.zero(), Complex.zero(), Complex.zero()},
                {Complex.zero(), Complex.unit(), Complex.zero(), Complex.zero()},
                {Complex.zero(), Complex.zero(), Complex.zero(), Complex.unit()},
                {Complex.zero(), Complex.zero(), Complex.unit(), Complex.zero()}
        };
        return result;
    }
}
