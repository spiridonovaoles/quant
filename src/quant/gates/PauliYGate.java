package quant.gates;

import quant.emulator.QuantumGate;
import quant.emulator.Complex;


public class PauliYGate extends QuantumGate {
    public PauliYGate (){
        this.qubitsNumber=1;
        this.size=2;
    }
    @Override
    public Complex[][] getMatrix() {
        Complex result [][] = {
                {Complex.zero(),new Complex(0, -1)},
                {new Complex(0,1),Complex.zero()}
        };
        return result;
    }
}
