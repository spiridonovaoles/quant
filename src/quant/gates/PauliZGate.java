package quant.gates;

import quant.emulator.Complex;
import quant.emulator.QuantumGate;


public class PauliZGate extends QuantumGate {
    public PauliZGate (){
        this.qubitsNumber=1;
        this.size=2;
    }
    @Override
    public Complex[][] getMatrix() {
        Complex result [][] = {
                {new Complex(0,1),Complex.zero()},
                {Complex.zero(),new Complex(0, -1)}
        };
        return result;
    }
}
