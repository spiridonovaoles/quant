package quant.gates;

import quant.emulator.Complex;
import quant.emulator.QuantumGate;


public class UGate extends QuantumGate {
    private Complex[][] matrix;
    public UGate (int qubitsNumber, Complex [][] uMatrix){
        this.qubitsNumber = qubitsNumber;
        this.size = (int) Math.pow(2, qubitsNumber);
        this.matrix = uMatrix;
    }
    @Override
    public Complex[][] getMatrix() {
        return matrix;
    }
}
